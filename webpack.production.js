const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

module.exports = merge(common, {
    mode: "production",
    plugins: [
        new webpack.DefinePlugin({
            BUILD_TIME: Date.now().toString(),
            LOCAL_INSTRUMENTS: false,
        }),
    ],
});
