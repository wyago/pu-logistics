var cacheName = 'js13kPWA-v1';

self.addEventListener("install", event => {
    console.log("installing...");
    event.waitUntil(
        caches
            .open(cacheName)
            .then(cache => {
                return cache.addAll([
                    "/index.html",
                    "/bundle.js",
                ]);
            })
            .catch(err => console.log(err))
    );
});

self.addEventListener('fetch', (e) => {
    e.respondWith((async () => {
      const r = await caches.match(e.request);
      console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
      if (r) { return r; }
      const response = await fetch(e.request);
      const cache = await caches.open(cacheName);
      console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
      cache.put(e.request, response.clone());
      return response;
    })());
  });