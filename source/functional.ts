

export function by(start: number, delta: number, count: number) {
    const result = [];
    for (let i = 0; i < count; ++i) {
        result.push(start);
        start += delta;
    }
    return result;
}

export function rotate<T>(array: T[], n: number): T[] {
    return array.map((_, i) => array[(i + n) % array.length]);
}

export function contains<T>(t: T, array: T[]): boolean {
    for (let i = 0; i < array.length; ++i) {
        if (array[i] === t) {
            return true;
        }
    }
    return false;
}

export function match<T>(left: readonly T[], right: readonly T[]): boolean {
    for (const l of left) {
        for (const r of right) {
            if (l === r) {
                return true;
            }
        }
    }
    return false;
}

export function shuffle<T>(array: T[]) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
}

export function index<T>(array: T[], indices: number[]): T[] {
    return indices.map(i => array[i]);
}

export function take<T>(array: T[], count: number): T[] {
    const result: T[] = new Array();
    for (let i = 0; i < count && i < array.length; ++i) {
        result[i] = array[i];
    }
    return result;
}

export function subArray<T>(array: T[], start: number, endBefore: number) {
    const result = new Array<T>(endBefore - start);
    for (let i = start; i < endBefore; ++i) {
        result[i - start] = array[i];
    }
    return result;
}

export function iota(length: number) {
    const result = new Array(length);
    for (let i = 0; i < length; ++i) {
        result[i] = i;
    }
    return result;
}

export function boolSetAccepts<T extends object>(expected: T, actual: T) {
    const keys = Object.keys(expected);
    for (let i = 0; i < keys.length; ++i) {
        if (!(expected as any)[keys[i]] !== !(actual as any)[keys[i]]) {
            return false;
        }
    }
    return true;
}

export function defaultValue<T>(value: T | undefined, default$: T): T {
    return value === undefined ? default$ : value;
}

export function maybeMap<T, Y>(value: T | undefined, f: (t: T) => Y): Y | undefined {
    return value === undefined ? undefined : f(value);
}

export function intercalate<T, Y = T>(array: T[], inBetween: () => Y): (T | Y)[] {
    const result = [];
    for (let i = 0; i < array.length; ++i) {
        result.push(array[i]);
        if (i !== array.length - 1) {
            result.push(inBetween());
        }
    }
    return result;
}

export function skip<T>(n: number, array: T[]): T[] {
    const result = new Array(array.length - n);
    for (let i = n; i < array.length; ++i) {
        result[i - n] = array[i];
    }
    return result;
}

export function join<T>(arrays: T[][]): T[] {
    return ([] as T[]).concat(...arrays);
}

export function clean<T>(array: (T | undefined)[]): T[] {
    return array.filter(x => x !== undefined) as T[];
}

export function randomOf<T>(array: T[]): T {
    return array[Math.floor(Math.random() * array.length)];
}

export function divide<T>(array: T[], index: number, insert: T) {
    if (array.length <= index) {
        return array;
    }

    const result = new Array(array.length + 1);
    for (let i = 0; i < result.length; ++i) {
        if (i < index) {
            result[i] = array[i];
        } else if (i === index) {
            result[i] = insert;
        } else {
            result[i] = array[i - 1];
        }
    }

    return result;
}

export function each<T, R>(object: T, f: <K extends keyof T & keyof R>(key: K, value: T[K]) => R[K]): R {
    const result: any = {};
    Object.getOwnPropertyNames(object).forEach(name => {
        result[name] = f(name as any, (object as any)[name]);
    })
    return result;
}