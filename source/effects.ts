export function assert(mustBeTrue: boolean): asserts mustBeTrue is true {
    if (!mustBeTrue) {
        throw new Error("It's not true!");
    }
}

export function existing<T>(mustExist: T | undefined): T {
    assert(mustExist !== undefined);
    return mustExist!;
}

export function never(): never {
    throw new Error("Never occurred");
}

export function wait(time: number): Promise<void> {
    return new Promise(resolve => window.setTimeout(resolve, time));
}
