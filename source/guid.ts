export type Guid = string & { __THIS_IS_A_GUID: boolean };

export function guid(custom: string | undefined = undefined) {
    return (custom ?? "00000000-0000-0000-0000-000000000000") as any as Guid;
}

export function isGuid(guid: any): guid is Guid {
    return typeof guid === "string";
}