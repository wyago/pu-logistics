import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { Component } from "../../types";
import { colors } from "../colors";
import { formClasses } from "./form";

export interface ToggleProps {
    readonly label?: string;
    readonly unit?: string;
    readonly defaultContents: string;
    readonly onchange?: (contents: string) => void;
}

export function numberInput({ label, defaultContents, unit, onchange }: ToggleProps): Component {
    let text = defaultContents;
    const key = Math.random();
    return {
        render: () => [
            label ? h("label." + css(formClasses.formLabel), {key}, [label]) : [],
            h("input." + css(classes.numberInput), {
                key,
                value: text,
                type: "text",
                onchange: (e) => onchange && onchange((e.target as HTMLInputElement).value)
            }),
            unit ? h("div." + css(classes.unit), [unit]) : undefined,
        ]
    }
}

const classes = StyleSheet.create({
    numberInput: {
        "grid-column": 2,
        padding: "0px 14px",
        "border-radius": "4px",
        "border-width": "1px",
        "outline": "none",
        height: 28,
        "&:active": {
            "border-color": colors.gray.normal
        },
        "&:focus": {
            "border-color": colors.blue.normal
        }
    },
    numberInputContainer: {
        display: "flex"
    },
    unit: {
        "grid-column": 3
    }
});
