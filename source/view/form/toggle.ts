import { css, StyleSheet } from "aphrodite";
import { h, VNodeChild } from "maquette";
import { colors } from "../colors";
import { globalClasses } from "../globalClasses";
import { formClasses } from "./form";

export interface ToggleProps {
    readonly key?: any;
    readonly label: VNodeChild;
    readonly disabled: boolean;
    readonly defaultEnabled: boolean;
    readonly onchange?: (enabled: boolean) => void;
}

export function toggle({ label, key, disabled, defaultEnabled, onchange }: ToggleProps) {
    let enabled = defaultEnabled;
    return {
        render: () => h("div." + css(classes.overall), key ? {key} : {}, [
            h("label." + css(formClasses.formLabel), [label]),
            h("div." + css(classes.toggle, disabled ? classes.disabled : undefined), {
                tabIndex: 0,
                onclick: () => (enabled = !enabled, onchange?.(enabled)),
                onkeydown: e => (e.key === " " ? (enabled = !enabled, onchange?.(enabled)) : undefined)
            }, enabled ? [h("img." + css(classes.indicator, globalClasses.introduced), {
                src: disabled ? "/icon/check-light.svg" : "/icon/check.svg"
            })] : []),
        ]),
        enabled: () => enabled
    }
}

const classes = StyleSheet.create({
    formLabel: {
        "grid-column": 1,
    },
    overall: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    toggle: {
        "grid-column": 2,
        cursor: "pointer",
        width: "28px",
        height: "28px",
        border: "solid 1px " + colors.gray.lighter,
        "border-radius": "4px",
        outline: "none",
        ":hover": {
            "background-color": colors.blue.lightest,
        },
        ":active": {
            "border-color": colors.gray.normal,
        },
        ":focus": {
            "border-color": colors.blue.normal,
        }
    },
    indicator: {
        margin: "1px",
        width: "24px",
        height: "24px",
    },
    disabled: {
        pointerEvents: "none",
        backgroundColor: colors.gray.lightest
    }
});
