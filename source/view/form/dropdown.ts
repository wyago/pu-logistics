import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { Component } from "../../types";
import { colors } from "../colors";

export interface DropdownProps {
    readonly options: string[];
    readonly def?: string;
    readonly onchange?: (option: string) => void;
}

export function dropdown({ options, def, onchange }: DropdownProps): Component {
    let value: string = def || "--";
    return {
        render: () => [
            h("select." + css(classes.dropdown), {
                onchange: e => {
                    value = (e.target as HTMLSelectElement).value;
                    onchange && onchange(value);
                },
                value
            }, options.map(option => h("option", [option]))),
        ]
    }
}

const classes = StyleSheet.create({
    formLabel: {
        "grid-column": 1,
    },
    dropdown: {
        "grid-column": 2,
        cursor: "pointer",
        outline: "none",
        "border-radius": "4px",
        margin: "0px 14px",
        height: 28,
        "&:active": {
            "border-color": colors.gray.normal
        },
        "&:focus": {
            "border-color": colors.blue.normal
        }
    }
});
