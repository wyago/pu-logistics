import { css, StyleSheet } from "aphrodite";
import { h, VNodeChild } from "maquette";
import { buildings, workforces } from "..";
import { globalProjector } from "../global";
import { Component } from "../types";
import { efficiency } from "./efficiency";
import { currency, formatted, time } from "./formatting";
import { globalClasses } from "./globalClasses";
import { material } from "./material";
import { getConsumption } from "./pages/createConsumptionPage";
import { price, unavailable } from "../model/price";
import { shimmer } from "./shimmer";

export let capexRatio = localStorage.getItem("capex") ? +localStorage.getItem("capex")! :1.00878;
export let compoundingPeriod = localStorage.getItem("compounding") ? ~~localStorage.getItem("compounding")! : 30;

export function setCapexRatio(ratio: string) {
    capexRatio = Math.pow(+ratio, 1/compoundingPeriod);
    localStorage.setItem("capex", (+capexRatio).toString());
}

export function setCompoundingPeriod(period: string) {
    compoundingPeriod = ~~period;
    localStorage.setItem("compounding", (compoundingPeriod).toString());
}

let workforceCosts: () => Promise<{ [workforce: string]: number }> = async () => {
    const workforceNames = Object.keys(workforces);
    const costs: { [workforce: string]: number } = {};
    for (const name of workforceNames) {
        costs[name] = await getConsumption(name);
    }
    return costs;
};

function buildingTime(process: Process): number {
    const days = (process.TimeMs / 1000 / 60 / 60 / 24); 
    if (!process.BuildingTicker) {
        return days;
    }

    return days / efficiency;
}

function costs(recipe: Process, buildingCost: number, inputPrices: number[], workforce: { [workforce: string]: number }): {
    total: number, 
    consumption: number, 
    inputs: number,
    capex: number
 } | {
     total: number
 } {
    const days = buildingTime(recipe);
    let consumption = 0;
    const inputCost = inputPrices.map((price, i) => price * recipe.Inputs[i].Amount).reduce((x, y) => x + y, 0);
    if (recipe.BuildingTicker) {
        consumption += buildings[recipe.BuildingTicker].Pioneers / 100 * workforce.PIONEER * days;
        consumption += buildings[recipe.BuildingTicker].Settlers / 100 * workforce.SETTLER * days;
        consumption += buildings[recipe.BuildingTicker].Technicians / 100 * workforce.TECHNICIAN * days;
        consumption += buildings[recipe.BuildingTicker].Engineers / 100 * workforce.ENGINEER * days;
        consumption += buildings[recipe.BuildingTicker].Scientists / 100 * workforce.SCIENTIST * days;
        const capex = (buildingCost*Math.pow(capexRatio, days) - buildingCost);
        return {
            total: capex + inputCost + consumption,
            consumption,
            inputs: inputCost,
            capex
        }
    } else {
        return { total: inputCost };
    }
}

export function profit(recipe: Process, buildingCost: number, inputPrices: number[], outputPrices: number[], workforce: { [workforce: string]: number }): { daily: number, margin: number } {
    const days = buildingTime(recipe);
    const revenue = outputPrices.map((o, i) => o * recipe.Outputs[i].Amount).reduce((x, y) => x + y, 0);
    const cogs = costs(recipe, buildingCost, inputPrices, workforce).total;
    return {
        daily: (revenue - cogs) / days ?? 0,
        margin: Math.pow(revenue/cogs, 1/days)
    }
}

export type Process = {
    BuildingTicker?: string;
    Inputs: {
        Ticker: string;
        Amount: number;
    }[];
    Outputs: {
        Ticker: string;
        Amount: number;
    }[];
    TimeMs: number;
}

function factor(count: number, ticker: string, direction: "output" | "input", onchange: (price: number, user: boolean) => void) {
    const priceRender = (p: number) => ({
        render: () =>
            h("div." + css(classes.optionalEdit), {
                contenteditable: "true",
                onkeyup: updateValue,
            }, [
                formatted(p)
            ])
    });

    function updateValue(e: Event) {
        const text = (e.target as HTMLDivElement).innerText;
        let newValue = Number.parseFloat((text.replace(/,/g, "").match(/\d+(\.\d+)?/) ?? [])[0]);
        if (text.endsWith("k") || text.endsWith("K")) {
            newValue *= 1000;
        } else if (text.endsWith("m") || text.endsWith("M")) {
            newValue *= 1000000;
        }
        if (newValue === newValue) {
            actualPrice = newValue;
            value.set(priceRender(newValue));
            onchange(actualPrice, true);
        }
    }
    
    const priceAsync = price(ticker);
    let actualPrice = 0;
    priceAsync.then(p => { actualPrice = p; onchange(p, false); globalProjector.scheduleRender() });
    const value = shimmer("value", "3em", "2em", priceAsync.then(priceRender));

    const { render } = shimmer("mat", "56px", "calc(56px + 2em + 20px)", unavailable(ticker).then(availability => ({
        render() {
            const warn = direction === "output" ? availability.bid : availability.ask;

            const materialPrice = title(
                h("div." + css(globalClasses.horizontal), { key: ticker + "title" }, [value.render(), currency()]), 
                "The typical price of " + ticker + " per unit");

            return direction === "output" ? [
                materialPrice,
                material(ticker, warn, count.toString()), 
            ] : [
                material(ticker, warn, count.toString()),
                materialPrice
            ];
        }
    })));
    
    return {
        value: () => actualPrice,
        render
    };
}

function title(text: VNodeChild, title: string) {
    return h("span", { key: title, title }, [text]);
}

export type RecipeComponent = {
    profitAsync(): Promise<number>,
    costsAsync(): Promise<number>,
    profit(): number,
    costs(): number,
    render(): VNodeChild
}

export function recipe(recipe: Process, nowrap = false): RecipeComponent {
    const inputFactors = recipe.Inputs.map(input => factor(input.Amount, input.Ticker, "input", update));
    const outputFactors = recipe.Outputs.map(output => factor(output.Amount, output.Ticker, "output", update));
    const workforce = workforceCosts();
    const buildingCost =
        recipe.BuildingTicker
            ? Promise.all(buildings[recipe.BuildingTicker].BuildingCosts.map(c => price(c.CommodityTicker)))
                .then(costs => costs.reduce((x, y) => x + y, 0))
            : Promise.resolve(0);

    let profitResult = 0;
    let costsResult = 0;
    let cost: Promise<{
        total: number;
        consumption: number;
        inputs: number;
        capex: number;
    } | {
        total: number;
    }> = undefined!;
    let profits: Promise<{
        daily: number;
        margin: number;
    }> = undefined!;

    let current: Component = { render: () => undefined };
    function update(input: number, user: boolean) {
        cost = Promise.all([buildingCost, workforce])
            .then(([buildingCost, workforce]) => costs(recipe, buildingCost, inputFactors.map(c => c.value()), workforce));
        profits = Promise.all([buildingCost, workforce])
            .then(([buildingCost, workforce]) => profit(recipe, buildingCost, inputFactors.map(i => i.value()), outputFactors.map(c => c.value()), workforce));

        profits.then(profit => !user ? profitResult = profit.daily : undefined);
        cost.then(cost => !user ? costsResult = cost.total : undefined);
        
        function formatShimmer(key: string, p: Promise<number>) {
            return shimmer(key, "4em", "2em", p.then(n => ({
                render: () => formatted(n)
            })));
        }

        const profitsComponent = formatShimmer("profits", profits.then(p => p.daily));
        const totalComponent = formatShimmer("total", cost.then(c => c.total));
        const perComponent = formatShimmer("per", cost.then(c => c.total / recipe.Outputs[0].Amount));

        const piecewiseComponent = shimmer("piecewise", "5em", "3em", cost.then(cost => ({
            render: () =>
                "inputs" in cost ? [
                    cost.inputs > 0 ? h("div", [title(["Inputs: ", formatted(cost.inputs)], "The cost of material inputs")]) : [],
                    h("div", { key: "consumption" }, [title(["Consumption: ", formatted(cost.consumption)], "The cost of consumable goods used by workers")]),
                    h("div", { key: "capex" }, [title(["Capex: ", formatted(cost.capex)], "The cost of the investment into construction of the building")])
                ] : [],
        })));
        current = {
            render() {
                return h("div." + css(classes.recipe), {key: recipe}, [
                    recipe.BuildingTicker ? [
                        h("h3", [
                            title([profitsComponent.render(), " ", currency(), " daily"], "The profits made by running this recipe for 24 hours"),
                        ]),
                        recipe.Outputs.length === 1
                            ? (h("p", [title(["Break even point: ", totalComponent.render()], "The output price which would create neither profit nor losses"), 
                                recipe.Outputs[0].Amount > 1 ? [" (", perComponent.render(), " each)"] : []]))
                            : recipe.Outputs.length > 0 ?  h("p", ["Break even point: ", totalComponent.render()]) : [],
                        piecewiseComponent.render(),
                        h("div." + css(classes.outputs), outputFactors.map(output =>
                            h("div." + css(classes.output), [output.render()]))),
                        outputFactors.length > 0 ? h("img", { src: "/icon/arrow-up.svg"}) : [],
                        h("div." + css(classes.time), [time(buildingTime(recipe)), " in a ", recipe.BuildingTicker]),
                        inputFactors.length > 0 ? h("img", { src: "/icon/line-up.svg"}) : [],
                    ] : [
                        h("p", ["Total cost: ", totalComponent.render()])
                    ],
                    h("div." + css(classes.inputs, !nowrap && classes.wrap), inputFactors.map(input =>
                        h("div." + css(classes.input), [input.render()])))
                ])
            }
        }
    }
    update(0, false);

    return {
        profitAsync: () => profits.then(p => p.daily),
        costsAsync: () => cost.then(c => c.total),
        profit: () => profitResult,
        costs: () => costsResult,
        render() {
            return current.render();
        }
    }
}

const classes = StyleSheet.create({
    time: {
        "margin-left": "14px",
        "margin-right": "14px",
    },
    recipe: {
        display: "flex",
        "flex-direction": "column",
        "align-items": "center",
        "border-radius": "6px",
        whiteSpace: "nowrap"
    },
    inputs: {
        display: "flex",
        "align-items": "center",
        "justify-content": "center",
    },
    wrap: {
        flexWrap: "wrap"
    },
    outputs: {
        display: "flex",
        "align-items": "center",
        "justify-content": "center",
    },
    input: {
        display: "flex",
        margin: "10px",
        "flexDirection": "column",
        "align-items": "center",
        "justify-content": "center",
        whiteSpace: "nowrap",
    },
    output: {
        display: "flex",
        margin: "10px",
        "flexDirection": "column",
        "align-items": "center",
        "justify-content": "center",
        whiteSpace: "nowrap",
    },
    optionalEdit: {
        marginRight: 8,
        height: "28px",
        lineHeight: "28px",
        ":hover": {
            backgroundColor: "#eee",
            borderBottom: "solid 1px #000",
        },
        ":focus": {
            outline: "none",
            borderBottom: "solid 1px #000",
        }
    }
});