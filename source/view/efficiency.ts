import { numberInput } from "./form/numberInput";

export let efficiency = localStorage.getItem("efficiency") ? +localStorage.getItem("efficiency")! : 1;
export function setEfficiency(e: number) {
    efficiency = e;
    localStorage.setItem("efficiency", efficiency.toString());
}

export function efficiencySetter(onchange?: () => void) {
    return numberInput({
        onchange: value => (setEfficiency(Number.parseFloat(value) / 100), onchange && onchange()),
        defaultContents: (efficiency * 100).toFixed(2)
    });
}