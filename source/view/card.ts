import { css, StyleSheet } from "aphrodite";
import { h, VNodeChildren, VNodeProperties } from "maquette";

const classes = StyleSheet.create({
    card: {
        "background-color": "white",
        "border-radius": "7px",
        padding: "14px 28px",
        margin: "14px 28px",
        position: "relative",
        boxShadow:`
  0 4.1px 5.3px rgba(0, 0, 0, 0.028),
  0 13.8px 17.9px rgba(0, 0, 0, 0.042),
  0 62px 80px rgba(0, 0, 0, 0.07)`,
        '@media (max-aspect-ratio: 2/3)': {
            margin: "6px",
            padding: "8px",
            width: "80vw"
        } ,
        width: "fit-content"
    }
});

export function card(properties: VNodeProperties = {}, children: VNodeChildren) {
    return h("div." + css(classes.card), {
        ...properties,
        classes: {
            ...properties.classes,
        }
    }, children);
}
