import { css, StyleSheet } from "aphrodite";
import { h, VNodeChild } from "maquette";
import { globalProjector } from "../global";
import { Component } from "../types";

export function shimmer(key: unknown, width: string, height: string, rendered: Promise<Component>) {
    let render: () => VNodeChild = () => h("div." + css(classes.shimmer), { key, styles: { width, height } }, [h("div." + css(classes.mover))]);
    
    rendered.then(r => {
        render = r.render;
        globalProjector.scheduleRender();
    });

    return {
        render: () => render(),
        set(child: Component) {
            render = child.render;
        }
    };
}

export function promiseDefault<T>(value: Promise<T>, def: T): () => T {
    let result = def;
    value.then(v => { result = v; globalProjector.scheduleRender() });
    return () => result;
}

const shimmerKeyframes = {
    "100%": {
        transform: "translateX(calc(50% + 100px))"
    }
}

const classes = StyleSheet.create({
    shimmer: {
        display: "inline-block",
        backgroundImage: "linear-gradient(to right, #ebebeb calc(50% - 100px), #c5c5c5 50%, #ebebeb calc(50% + 100px))",
        position: "relative",
        overflow: "hidden",
        margin: "0px 5px"
    },
    mover: {
        position: "absolute",
        top: "0",
        right: "0",
        width: "calc(200% + 200px)",
        bottom: "0",
        backgroundImage: "inherit",
        animationName: [shimmerKeyframes],
        animationDuration: "2s", 
        animationIterationCount: "infinite",
    }
});