import { css, StyleSheet } from "aphrodite";
import { h, VNodeChild } from "maquette";

const classes = StyleSheet.create({
    root: {
        display: "flex",
        height: "100%",
        width: "100%",
        '@media (max-aspect-ratio: 2/3)': {
            flexDirection: "column-reverse"
        }
    }
});

export function root(children: VNodeChild[]) {
    return h("div." + css(classes.root), {}, children);
}