import { css, StyleSheet } from "aphrodite/no-important";
import { h, VNodeChild, VNodeProperties } from "maquette";
import { colors } from "./colors";

export function button(properties: VNodeProperties = {}, children: VNodeChild[] = []) {
    return h("button", {
        ...properties,
        classes: {
            [css(classes.buttonDefault)]: true,
            ...properties.classes,
        }
    }, children);
}

const classes = StyleSheet.create({
    buttonDefault: {
        position: "relative",
        display: "flex",
        "align-items": "center",
        padding: "0 14px",
        "border-top": "none",
        "border-right": "none",
        "border-left": "none",
        "border-bottom": "none",
        outline: "none",
        height: "48px",
        "line-height": "48px",
        "background-color": "transparent",
        "font-size": "14px",
        transition: "transform 0.05s",
        cursor: "pointer",
        "&:hover": {
            transform: "translate(0, 2px)",
            "background-color": colors.blue.lightest,
        },
        "&:active": {
            transform: "translate(0, 4px)",
            "z-index": 1
        },
        "&:focus": {
            "box-shadow": "0 0 0 2px " + colors.blue.normal
        },
    },
});