import { StyleSheet } from "aphrodite";

export const globalClasses = StyleSheet.create({
    notRendered: {
        border: "0",
        clip: "rect(1px, 1px, 1px, 1px)",
        height: "1px",
        margin: "-1px",
        overflow: "hidden",
        padding: "0",
        position: "absolute",
        width: "1px",
    },
    "@keyframes intro": {
        '0%': {
            opacity: 0,
        },
    
        '100%': {
            opacity: 1,
        }
    },
    "@keyframes intro-down": {
        '0%': {
            opacity: 0,
            transform: 'translate(0, -14px)'
        },
    
        '99%': {
            opacity: 1,
            transform: 'translate(0, 0px)'
        },
    
        '100%': {
            opacity: 1,
        }
    },
    introduced: {
        animation: 'intro 0.2s cubic-bezier(0.33, 1, 0.68, 1)'
    },
    introducedDown: {
        animation: 'intro-down 0.2s cubic-bezier(0.33, 1, 0.68, 1)',
    },
    page: {
        display: "flex",
        'flex-direction': "column",
        padding: "28px",
        overflow: "auto",
        height: "100%",
        width: "100%",
        '@media (max-aspect-ratio: 2/3)': {
            padding: "14px"
        } 
    },
    horizontal: {
        display: "flex"
    },
    wrap: {
        display: "flex",
        flexWrap: "wrap"
    },
    column: {
        display: "flex",
        'flex-direction': "column",
        width: "fit-content"
    }
});