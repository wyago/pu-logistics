import { css, StyleSheet } from "aphrodite";
import { h, VNode } from "maquette";
import { button } from "./button";

const classes = StyleSheet.create({
    menu: {
        display: "flex",
        "flex-direction": "column",
        "justify-content": "flex-start",
        flex: "0",
        "background-color": "white",
        height: "100%",
        '@media (max-aspect-ratio: 2/3)': {
            flexDirection: "row",
            justifyContent: "space-between"
        }
    },
    buttonText: {
        "margin-left": "14px",
        "margin-right": "14px",
        "white-space": "nowrap",
        '@media (max-aspect-ratio: 2/3)': {
            display: "none"
        }
    },
    noCell: {
        '@media (max-aspect-ratio: 2/3)': {
            display: "none"
        } 
    }
});

function menuItem({ title, src, expanded, destination, onclick, noCell }: { title: string, src: string, expanded: boolean, noCell?: boolean, destination?: string, onclick?: () => void }) {
    return button({
        onclick: () => {
            if (destination) {
                window.history.pushState(undefined, title, destination);
            }
            onclick?.();
        },
        onauxclick: (e: MouseEvent) => {
            if (e.button !== 1) {
                return;
            }
            window.open(destination);
        },
        styles: { width: expanded ? "" : "48px" },
        classes: {
            [css(classes.noCell)]: !!noCell
        }
    }, [
        h("img", { title, src, width: 24, height: 24 }),
        expanded ? h("div." + css(classes.buttonText), [title]) : undefined
    ]);
}

export function createMenu() {
    function onexpand() {
        expanded = !expanded;
        localStorage.setItem("menu.expanded", expanded ? "true" : "false");
        render();
    }

    let menu: VNode;
    let expanded: boolean = localStorage.getItem("menu.expanded") === "true";
    
    function render() {
        menu = h("div." + css(classes.menu), [
            menuItem({ title: "Expand", src: "/icon/menu.svg", expanded, noCell: true, onclick: onexpand }),
            menuItem({ title: "Building use", src: "/icon/home.svg", expanded, destination: "/building" }),
            menuItem({ title: "Supply chain", src: "/icon/process.svg", expanded, destination: "/supplychain" }),
            menuItem({ title: "Consumption", src: "/icon/coffee.svg", expanded, destination: "/consumption" }),
            menuItem({ title: "About", src: "/icon/help-circle.svg", expanded, destination: "/about" }),
            menuItem({ title: "Settings", src: "/icon/settings.svg", expanded, destination: "/settings" }),
        ]);
    }
    render();

    return () => menu;
}