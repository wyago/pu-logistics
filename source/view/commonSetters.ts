import { css } from "aphrodite";
import { h } from "maquette";
import { efficiencySetter } from "./efficiency";
import { exchangeSetter } from "./exchange";
import { globalClasses } from "./globalClasses";
import { tickerSearch } from "./tickerSearch";

export function commonSetters(tickers: { Name: string, Ticker: string}[], updateTicker: (ticker?: string) => void, def: string) { 
    tickers.sort((a, b) => a.Ticker.localeCompare(b.Ticker));
    const search = tickerSearch(
        def || "",
        tickers,
        updateTicker);

    if (def) {
        updateTicker(def);
    }

    const changeExchange = exchangeSetter(updateTicker);
    const changeEfficiency = efficiencySetter(updateTicker);

    return {
        render: () => [
            h("div." + css(globalClasses.horizontal), {key: "sr"}, [
                h("label", { styles: { width: (14 * 5) + "px", marginBottom: "14px"} }, ["Ticker: "]),
                search(),
            ]),
            h("div." + css(globalClasses.horizontal), {key: "ex"}, [
                h("label", { styles: { width: (14 * 5) + "px", marginBottom: "14px"} }, ["Exchange: "]),
                changeExchange.render(),
            ]),
            h("div." + css(globalClasses.horizontal), {key: "ef"}, [
                h("label", { styles: { width: (14 * 5) + "px", marginBottom: "14px"} }, ["Efficiency: "]),
                changeEfficiency.render(),
            ])
        ]
    };
}