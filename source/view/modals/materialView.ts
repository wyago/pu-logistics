import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { buildingList, exchanges, materials, recipes, workforceList } from "../..";
import { price } from "../../model/price";
import { Component } from "../../types";
import { card } from "../card";
import { formatted, formattedInteger, time } from "../formatting";
import { material } from "../material";
import { shimmer } from "../shimmer";

let materialModal: string | undefined = undefined;
let prices: Component[] = [];

export function setMaterialModal(ticker: string) {
    materialModal = ticker;

    prices = exchanges.map(cx =>
        shimmer(cx, "3em", "1em", price(ticker, cx.ExchangeCode)
            .then(p => ({
                render: () => h("div", [formatted(p), " ", h("span." + css(classes.smaller), [cx.CurrencyCode])])
            }))));
}

export function createMaterialView(): Component {
    function exit(event: Event) {
        if ((event.target as HTMLElement).className === css(classes.modalContainer) ||
            (event.target as HTMLElement).className === css(classes.exit)) {
            materialModal = undefined;
        }
    }

    return {
        render() {
            if (materialModal === undefined) {
                return undefined;
            }
            const mat = materials[materialModal];
            const production = recipes.filter(r => r.Outputs.some(o => o.Ticker === materialModal))
                .map(r => ({ ...r, Name: r.BuildingTicker, Time: time(r.TimeMs / 1000 / 60 / 60 / 24)}));
            const products = recipes.filter(r => r.Inputs.some(i => i.Ticker === materialModal))
                .map(r => ({ ...r, Name: r.BuildingTicker, Time: time(r.TimeMs / 1000 / 60 / 60 / 24)}));
            const buildingUses = buildingList.filter(b => b.BuildingCosts.some(c => c.CommodityTicker === materialModal));
            const consumers = workforceList.filter(w => w.Needs.some(n => n.MaterialTicker === materialModal));

            return h("div." + css(classes.modalContainer), {
                onclick: exit
            }, [
                card({
                    classes: {
                        [css(classes.modal)]: true,
                    }
                }, [
                    h("div." + css(classes.title), [h("h1", [materialModal, ": ", mat.Name]), h("button." + css(classes.exit), { onclick: exit}, ["x"])]),
                    h("div", ["Weight: ", formatted(mat.Weight), "t"]),
                    h("div", ["Volume: ", formatted(mat.Volume), "m^3"]),
                    h("div", ["Shipload: ", formattedInteger(500 / Math.max(mat.Weight, mat.Volume))]),

                    h("h2", ["Prices"]),
                    h("div." + css(classes.priceGrid), prices.map(p => h("div", [p.render()]))),
                    production.length === 0 ? [] : [
                        h("h2", {key: "production"}, ["Production"]),
                        recipeList(production)
                    ],
                    products.length === 0 ? [] : [
                        h("h2", {key: "products"}, ["Products"]),
                        recipeList(products)
                    ],
                    buildingUses.length === 0 ? [] : [
                        h("h2", {key: "buildings"}, ["Buildings"]),
                        recipeList(buildingUses.map(b => ({ Inputs: b.BuildingCosts.map(c => ({ Ticker: c.CommodityTicker, Amount: c.Amount})), Outputs: [{ Ticker: b.Ticker, Amount: 1 }] })))
                    ],
                    consumers.length === 0 ? [] : [
                        h("h2", {key: "consumers"}, ["Consumers"]),
                        recipeList(consumers.map(b => ({
                            Inputs: b.Needs.map(n => ({ Ticker: n.MaterialTicker, Amount: n.Amount })),
                            Outputs: [{ Ticker: b.WorkforceType, Amount: 100 }],
                            Time: "1d"
                        })), formatted)
                    ],
                ])
            ]);
        }
    }
}

function recipeList(recipes: { Name?: string, Time?: string, Inputs: { Ticker: string, Amount: number }[], Outputs: { Ticker: string, Amount: number }[] }[], formatter?: (n: number) => string) {
    formatter = formatter || formatted;
    return h("div." + css(classes.recipeList), {key: Math.random()},
        recipes
            .map(recipe =>
                h("div." + css(classes.recipe), {
                    key: recipe
                }, [
                    h("div." + css(classes.inputs), recipe.Inputs.map(i => material(i.Ticker, false, formatter!(i.Amount)))),
                    recipe.Time ? [
                        h("img", {src: "/icon/line-right.svg"}),
                        h("div." + css(classes.name), [recipe.Name ? [recipe.Name, " "] : [], recipe.Time]),
                        h("img", {src: "/icon/arrow-right.svg"}),
                    ] : h("img", {src: "/icon/arrow-right.svg"}),
                    h("div." + css(classes.outputs), recipe.Outputs.map(i => material(i.Ticker, false, formatter!(i.Amount))))
                ])));
}

const classes = StyleSheet.create({
    modalContainer: {
        backgroundColor: "rgba(0,0,0,0.1)",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        left: "0",
        right: "0",
        top: "0",
        bottom: "0"
    },
    name: {
        heigth: "1em",
    },
    priceGrid: {
        display: "grid",
        gridTemplateColumns: "1fr 1fr",
        width: "fit-content",
        ">div": {
            padding: "2px"
        }
    },
    modal: {
        maxHeight: "80vh",
        minWidth: "600px",
        overflowY: "auto"
    },
    title: {
        display: "flex",
        justifyContent: "space-between",
    },
    exit: {
        background: "none",
        border: "none",
        fontSize: "150%",
        cursor: "pointer"
    },
    smaller: {
        fontSize: "80%"
    },
    recipeList: {
        backgroundColor: "#eff5f8",
        borderRadius: "8px",
        boxShadow:
        `inset 0px 2.6px 2.5px rgba(0, 0, 100, 0.028),
        inset 0px 8.7px 8.3px rgba(0, 0, 100, 0.042),
        inset 0px 39px 37px rgba(0, 0, 100, 0.07)`,
        padding: "14px",
    },
    recipe: {
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center"
    },
    inputs: {
        display: "flex",
        justifyContent: "flex-end"
    },
    outputs: {
        display: "flex",
    }
})