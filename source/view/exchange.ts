import { dropdown } from "./form/dropdown";

export let exchange = localStorage.getItem("exchange") || "CI1";
export function setExchange(name: string) {
    exchange = name;
    localStorage.setItem("exchange", name);
}

export function exchangeSetter(onchange?: () => void) {
    return dropdown({
        options: ["CI1","AI1","NC1","IC1"],
        onchange: value => (setExchange(value), onchange && onchange()),
        def: exchange
    });
}