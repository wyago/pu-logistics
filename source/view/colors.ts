export const colors = {
    red: {"darkest":"#2a0111","darker":"#630828","dark":"#a21535","normal":"#e82325","light":"#f17b6a","lighter":"#f1b9ad","lightest":"#faeeeb"},
    yellow: {"darkest":"#261400","darker":"#6a4800","dark":"#b28400","normal":"#fcc800","light":"#fcdb66","lighter":"#fceca7","lightest":"#fefbe5"},
    blue: {"darkest":"#22022b","darker":"#4a0d74","dark":"#5423e0","normal":"#2f71ec","light":"#54a3ed","lighter":"#9bcdf0","lightest":"#e5f3fa"},
    green: {"darkest":"#01140f","darker":"#073826","dark":"#145f34","normal":"#278922","light":"#74ad43","lighter":"#b2d268","lightest":"#eff893"},
    gray: {"darkest":"#170d1b","darker":"#362c44","dark":"#534f6e","normal":"#737685","light":"#999ea7","lighter":"#c2c7cb","lightest":"#eff1f2"},
} as const;