import { Component } from "../types";
import { create404Page } from "./pages/create404Page";
import { createAboutPage } from "./pages/createAboutPage";
import { createBuildingPage } from "./pages/createBuildingPage";
import { createConsumptionPage } from "./pages/createConsumptionPage";
import { createFundamentalsPage } from "./pages/createFundamentalsPage";
import { createSettingsPage } from "./pages/createSettingsPage";
import { createSupplyChainPage } from "./pages/createSupplyChainPage";

function createSiteMap(mapping: [string[], (params: any) => Component][]): (current: Function, page: Component, path: string) => Component | undefined {
    return (current: Function, page: Component, path: string) => {
        if (path === "/") {
            return createBuildingPage({ ticker: undefined });
        }

        const split = path.substring(1).split("/");
        outer:
        for (const pair of mapping) {
            const [expected, construct] = pair;
            if (split.length !== expected.length) {
                continue;
            }

            const params: { [name: string]: string } = {};
            for (let i = 0; i < expected.length; ++i) {
                if (expected[i].startsWith("$")) {
                    params[expected[i].substring(1)] = split[i];
                } else {
                    if (expected[i] !== split[i]) {
                        continue outer;
                    }
                }
            }

            if (construct === current) {
                return page;
            }
            return construct(params);
        }

        return undefined;
    }
}

const siteMap = createSiteMap([
    [["building"], createBuildingPage],
    [["building", "$ticker"], createBuildingPage],
    [["supplychain"], createSupplyChainPage],
    [["supplychain", "$ticker"], createSupplyChainPage],
    [["fundamentals"], createFundamentalsPage],
    [["fundamentals", "$ticker"], createFundamentalsPage],
    [["consumption"], createConsumptionPage],
    [["about"], createAboutPage],
    [["settings"], createSettingsPage],
]);

export function createMainPane() {
    let currentPath: string | undefined = undefined;
    let page: Component = createSettingsPage();
    let current: Function = () => {};

    return () => {
        if (currentPath != window.location.pathname) {
            const newPage = siteMap(current, page, window.location.pathname) ?? create404Page();
            if (newPage === page) {
                return;
            }
            page = newPage;
        }
        currentPath = window.location.pathname;
        return page.render()
    }
}