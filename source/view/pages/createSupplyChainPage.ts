import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { buildingList, buildings, materialList, materials, recipes } from "../..";
import { intercalate } from "../../functional";
import { globalProjector } from "../../global";
import { Component } from "../../types";
import { card } from "../card";
import { commonSetters } from "../commonSetters";
import { globalClasses } from "../globalClasses";
import { isFundamental } from "../material";
import { Process, recipe, RecipeComponent } from "../recipe";

export function createSupplyChainPage({ ticker }: { ticker: string | undefined }): Component {
    let rootRecipes: Process[] = [];
    let list: Component = { render: () => []};

    const updateMaterial = (input?: string) => {
        input = input ?? def;
        input = input.toUpperCase();
        if(materials[input] || buildings[input]) {
            if (input.length > 0) {
                history.replaceState(null, "", "/supplychain/" + input);
            } else {
                history.replaceState(null, "", "/supplychain");
            }
            localStorage.setItem("supplychain", input);
        }
        if (materials[input]) {
            rootRecipes = recipes.filter(r => r.Outputs.some(o => o.Ticker == materials[input!].Ticker));
        }  else if (buildings[input]) {
            const building = buildings[input];
            rootRecipes = [{
                Inputs: building.BuildingCosts.map(c => ({
                    Amount: c.Amount,
                    Ticker: c.CommodityTicker
                })),
                Outputs: [],
                TimeMs: 0
            }];
        }
        list = chain(rootRecipes);
    }

    const def = ticker || localStorage.getItem("supplychain") || "";
    const setters = commonSetters(
        (materialList as { Name: string, Ticker: string}[]).concat(buildingList),
        updateMaterial,
        def);

    return {
        render: () => h("div." + css(globalClasses.page), [
            h("h1", ["Supply chain"]),
            h("p", ["Type a material, building, or POPI ticker, and this will show the full series of recipes that are involved in its creation. This page is less complete than the building use page, but it's interesting to look at. I'm hoping to make a more useful tree view with the value-added at each step."]),
            setters.render(),
            list.render()
        ])
    };
}

function chain(sources: Process[]): Component {
    const components: RecipeComponent[][] = []

    components.push(sources.map(r => recipe(r, true)));

    function addNext(sources: Process[]) {
        (async () => {
            const next: Process[] = [];
            for (const sublist of sources.map(newRoot => newRoot.Inputs.filter(i => !isFundamental(materials[i.Ticker])).map(i => recipes.filter(r => r.Outputs.some(o => o.Ticker == i.Ticker))!))) {
                for (const subsublist of sublist) {
                    let max = Number.MIN_VALUE;
                    let insert: Process = subsublist[0];
                    for (const item of subsublist) {
                        const p = await recipe(item).profitAsync();
                        if (max < p) {
                            max = p;
                            insert = item;
                        }
                    }
    
                    if (insert && !next.some(x => x === insert)) {
                        next.push(insert);
                    }
                }
            }
            if (next.length > 0) {
                components.push(next.map(r => recipe(r, true)));
                addNext(next);
            }
        })().then(globalProjector.scheduleRender);
    }
    addNext(sources);

    return {
        render: () => intercalate(
            components.map(cs => h("div." + css(classes.step), {key: cs}, cs.map(c => card({}, [c.render()])))),
            () => h("div." + css(classes.arrow), {key: Math.random()}, [h("img", { src: "/icon/arrow-up.svg" })]))
    };
}

const classes = StyleSheet.create({
    step: {
        display: "flex",
        "align-items": "center",
    },
    arrow: {
        marginLeft: "48px",
        color: "#fff"
    }
});