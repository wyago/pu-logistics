import { css } from "aphrodite";
import { h } from "maquette";
import { Component } from "../../types";
import { clearCache } from "../../model/cache";
import { numberInput } from "../form/numberInput";
import { globalClasses } from "../globalClasses";
import { capexRatio, compoundingPeriod, setCapexRatio, setCompoundingPeriod } from "../recipe";
import { clearStorage } from "../../model/storage";

export function createSettingsPage(): Component {
    let capex = numberInput({
        label: "Time-value of money over the following period, as a multiplier",
        defaultContents: "" + (Math.pow(capexRatio, compoundingPeriod)).toFixed(3),
        onchange: setCapexRatio
    });

    const period = numberInput({
        label: "Length of the period to compound over",
        defaultContents: "" + compoundingPeriod,
        onchange: v => {
            setCompoundingPeriod(v);
            capex = numberInput({
                label: "Time-value of money over the following period",
                defaultContents: "" + (Math.pow(capexRatio, compoundingPeriod)).toFixed(3),
                onchange: setCapexRatio
            });
        }
    });

    return {
        render: () => h("div." + css(globalClasses.page), [
            h("div." + css(globalClasses.column), [
                h("h1", ["Settings"]),
                h("p"),
                h("button", {
                    onclick: () => { clearCache(); }
                }, ["Clear data cache"]),
                h("button", {
                    onclick: () => { clearStorage(); }
                }, ["Clear all settings and cache"]),
                capex.render(),
                period.render(),
            ])
        ])
    }
}