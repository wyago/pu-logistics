import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { buildingList, buildings, materialsById, planetByNaturalId, planetList, recipes } from "../..";
import { Building } from "../../coreTypes";
import { Component } from "../../types";
import { card } from "../card";
import { commonSetters } from "../commonSetters";
import { globalClasses } from "../globalClasses";
import { Process, recipe } from "../recipe";
import { tickerSearch } from "../tickerSearch";

export function createBuildingPage({ ticker }: { ticker: string | undefined }): Component {
    const def = ticker || localStorage.getItem("building") || "";

    const update = (input?: string) => {
        input = input ?? def;
        input = input.toUpperCase();
        if (buildings[input]) {
            if (input.length > 0) {
                history.replaceState(null, "", "/building/" + input);
            } else {
                history.replaceState(null, "", "/building");
            }
            localStorage.setItem("building", input);
            sorted = sortedRecipes(buildings[input]);
        }
    }

    let sorted: Component = { render: () => []};
    const setters = commonSetters(buildingList.filter(b => b.Recipes.length > 0), update, def);

    return {
        render: () => h("div." + css(globalClasses.page), {}, [
            h("h1", ["Building use"]),
            h("p", [
                "Type a building ticker, and this will show the possible recipes sorted in order of expected profit. ",
                "This doesn't take into account volume however, so make sure to sanity check the results."
            ]),
            setters.render(),
            sorted.render()
        ])
    }
}

function sortedRecipes(building: Building): Component {
    const creatable = recipes.filter(r => r.BuildingTicker === building.Ticker);
    if (creatable.length === 1 && creatable[0].Outputs.length === 0) {
        return extractor(building, creatable[0]);
    }

    const priced = creatable.map(r => recipe(r));

    return {
        render: () => h("div." + css(classes.recipes), { styles: { display: "flex", flexWrap: "wrap" } }, 
            priced.sort((a, b) => a.profit() === b.profit() ? 0 : (a.profit() > b.profit() ? -1 : 1)).map(p => card({key: p}, [p.render()])))
    };
}

function extractionType(building: Building) {
    return building.Ticker === "RIG"
        ? "LIQUID" : 
        building.Ticker === "EXT"
        ? "MINERAL" :
        "GASEOUS";
}

function extractor(building: Building, process: Process): Component {
    const update = (input: string) => {
        if (planetByNaturalId[input]) {
            const all = planetByNaturalId[input].Resources
                .filter(r => r.ResourceType === extractionType(building))
                .map(r => recipe({
                    Inputs: [],
                    Outputs: [{ Ticker: materialsById[r.MaterialId].Ticker, Amount: Math.round(r.Factor * (r.ResourceType === "GASEOUS" ? 60 : 70) * (process.TimeMs / 1000 / 60 / 60 / 24))}],
                    TimeMs: process.TimeMs,
                    BuildingTicker: building.Ticker
                }));
            const list: Component[] = all.length > 0 ? all : [{ render: () => h("p", ["No resources of this type on the selected planet."]) }];
            possible = {
                render: () => h("div." + css(globalClasses.column), list.map(p => card({key: p}, [p.render()])))
            };
        }
    }

    const search = tickerSearch(
        "",
        planetList.map(p => ({ Name: p.Name, Ticker: p.Id })),
        update);
    let possible: Component = { render: () => [] };

    return {
        render: () => h("div", [
            h("div." + css(globalClasses.horizontal), {}, [
                h("label", { styles: { width: (14 * 5) + "px"} }, ["Planet: "]),
                search(),
            ]),
            possible.render()
        ])
    };
}

const classes = StyleSheet.create({
    recipes: {
        display: "flex",
        flexWrap: "wrap",
        '@media (max-aspect-ratio: 2/3)': {
            justifyContent: "center"
        } 
    }
})