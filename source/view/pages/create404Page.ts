import { css } from "aphrodite";
import { h } from "maquette";
import { Component } from "../../types";
import { globalClasses } from "../globalClasses";

export function create404Page(): Component {
    return {
        render: () => h("div." + css(globalClasses.page), [
            h("h1", ["404 error"]),
            h("p", ["Couldn't find a page for this URL"])
        ])
    }
}