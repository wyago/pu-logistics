import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { Component } from "../../types";
import { globalClasses } from "../globalClasses";

export function createProjectsPage(): Component {    
    return {
        render: () => h("div." + css(globalClasses.page), {}, [
            h("h1", ["Projects"]),
        ])
    }
}

const classes = StyleSheet.create({
    recipes: {
        display: "flex",
        flexWrap: "wrap",
        '@media (max-aspect-ratio: 2/3)': {
            justifyContent: "center"
        }
    }
})