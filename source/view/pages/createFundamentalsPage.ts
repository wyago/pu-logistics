import { css, StyleSheet } from "aphrodite";
import { h } from "maquette";
import { buildings, materialList, materials, recipes, workforces } from "../..";
import { Component } from "../../types";
import { card } from "../card";
import { commonSetters } from "../commonSetters";
import { efficiency } from "../efficiency";
import { currency, formatted, time } from "../formatting";
import { globalClasses } from "../globalClasses";
import { isFundamental, material } from "../material";
import { price, unavailable } from "../../model/price";
import { Process, recipe } from "../recipe";
import { shimmer } from "../shimmer";

export function createFundamentalsPage({ ticker }: { ticker: string | undefined }): Component {
    const updateMaterial = (input?: string) => {
        input = input ?? def;
        input = input.toUpperCase();
        if(materials[input] && !isFundamental(materials[input]) || buildings[input]) {
            if (input.length > 0) {
                history.replaceState(null, "", "/fundamentals/" + input);
            } else {
                history.replaceState(null, "", "/fundamentals");
            }
            localStorage.setItem("fundamentals", input);
        }

        if (materials[input] && !isFundamental(materials[input])) {
            list = contents([[input, 1]]);
        }  else if (buildings[input]) {
            list = contents(buildings[input].BuildingCosts.map(c => [c.CommodityTicker, c.Amount] as const));
        }
    }

    let list: Component = { render: () => [] };
    const def = ticker || localStorage.getItem("fundamentals") || "";
    const setters = commonSetters(
        (materialList as { Name: string, Ticker: string}[])
            .filter(m => !isFundamental(materials[m.Ticker]))
            .sort((a, b) => a.Ticker.localeCompare(b.Ticker)),
        updateMaterial,
        def);

    return {
        render: () => h("div." + css(globalClasses.page), [
            h("h1", ["Fundamental costs"]),
            h("p", ["Type a material, and this will show the raw resources and asset time required to create the final result."]),
            setters.render(),
            list.render()
        ])
    };
}

function contents(desired: (readonly [string, number])[]): Component {
    const priceMap = Promise.all(desired.map(d => gather(d[0], d[1])))
        .then(desires =>
            desires.reduce((x, y) => ({
                buildingTime: mergeAdd(x.buildingTime, y.buildingTime),
                materialCount: mergeAdd(x.materialCount, y.materialCount)
            }), { buildingTime: {}, materialCount: {} }))
        .then(({ buildingTime, materialCount }) => {
            const buildingNames = Object.keys(buildingTime).sort();
        
            for (const building of buildingNames) {
                if (buildings[building].Pioneers) {
                    for (const need of workforces.PIONEER.Needs) {
                        materialCount[need.MaterialTicker] = (materialCount[need.MaterialTicker] ?? 0) + need.Amount * buildings[building].Pioneers / 100 * buildingTime[building];
                    }
                }
                if (buildings[building].Settlers) {
                    for (const need of workforces.SETTLER.Needs) {
                        materialCount[need.MaterialTicker] = (materialCount[need.MaterialTicker] ?? 0) + need.Amount * buildings[building].Settlers / 100 * buildingTime[building];
                    }
                }
                if (buildings[building].Technicians) {
                    for (const need of workforces.TECHNICIAN.Needs) {
                        materialCount[need.MaterialTicker] = (materialCount[need.MaterialTicker] ?? 0) + need.Amount * buildings[building].Technicians / 100 * buildingTime[building];
                    }
                }
                if (buildings[building].Engineers) {
                    for (const need of workforces.ENGINEER.Needs) {
                        materialCount[need.MaterialTicker] = (materialCount[need.MaterialTicker] ?? 0) + need.Amount * buildings[building].Engineers / 100 * buildingTime[building];
                    }
                }
                if (buildings[building].Scientists) {
                    for (const need of workforces.SCIENTIST.Needs) {
                        materialCount[need.MaterialTicker] = (materialCount[need.MaterialTicker] ?? 0) + need.Amount * buildings[building].Scientists / 100 * buildingTime[building];
                    }
                }
            }

            const materialNames = Object.keys(materialCount).sort().sort((x, y) => materials[x].CategoryName.localeCompare(materials[y].CategoryName));
        
            return Promise.all(materialNames.map(m => price(m)))
                .then(priceList => {
                    const priceMap: { [name: string]: number } = {};
                    materialNames.forEach((name, i) => {
                        priceMap[name] = priceList[i];
                    });
                    return {
                        priceMap,
                        materialCount,
                        materialNames,
                        buildingNames,
                        buildingTime
                    };
                });
        });

    return shimmer("fundamental", "calc(100% - 50px)", "300px", priceMap.then(({ priceMap, materialCount, materialNames, buildingNames, buildingTime }) => ({
        render() {
            const priceDisplay = formatted(materialNames.map(m => materialCount[m] * priceMap[m]).reduce((x, y) => x + y, 0));
            return [
                card({key: 1}, [
                    h("h2", ["Total of ", priceDisplay, " ", currency()]),
                    h("div." + css(globalClasses.wrap),
                        materialNames
                            .filter(m => priceMap[m] * materialCount[m] > 0.01)
                            .map(m => h("div." + css(classes.material), { key: m }, [
                                material(m),
                                h("div", [formatted(materialCount[m])]),
                                h("div", [formatted(priceMap[m] * materialCount[m]) + " " + currency()]),
                                h("div", [formatted(priceMap[m]), " ", currency() + " each"]),
                            ])))
                ]),
                card({ key: 2 }, [
                    h("h2", ["Total of ", time(buildingNames.map(b => buildingTime[b]).reduce((x, y) => x + y, 0))]),
                    h("div." + css(globalClasses.wrap),
                        buildingNames.sort((x, y) => buildingTime[x] < buildingTime[y] ? 1 : -1).map(b => h("div." + css(classes.material), { key: b }, [
                            material(b),
                            h("div", [time(buildingTime[b])]),
                        ])))
                ]),
            ];
        }
    })));
}

async function gather(desired: string, count: number): Promise<{ buildingTime: { [building: string]: number }, materialCount: { [material: string]: number } }> {
    const buildingTime: { [building: string]: number } = {};
    const materialCount: { [material: string]: number } = {};

    const rs = recipes.filter(r => r.Outputs.some(o => o.Ticker == desired));
    let process: Process = rs[0];
    let min = Number.MAX_VALUE;
    for (const item of rs) {
        const penalize = (await Promise.all(
            item.Inputs
                .map(i => unavailable(i.Ticker).then(x => x.ask))))
                .some(x => x);
        
        const costs = await recipe(item).costs() / item.Outputs.find(o => o.Ticker === desired)!.Amount - (penalize ? 1000000000 : 0)
        if (min < costs) {
            min = costs;
            process = item;
        }
    }

    const output = process.Outputs.find(o => o.Ticker === desired)!.Amount;

    if (process.BuildingTicker) {
        buildingTime[process.BuildingTicker] = (buildingTime[process.BuildingTicker] ?? 0) + 
            ((process.TimeMs / 1000 / 60 / 60 / 24) * count / output / efficiency);
    }
    process.Inputs
        .filter(i => isFundamental(materials[i.Ticker]))
        .forEach(i => materialCount[i.Ticker] = (materialCount[i.Ticker] ?? 0) + i.Amount * count / output);

    return (await Promise.all(process.Inputs
        .filter(i => !isFundamental(materials[i.Ticker]))
        .map(i => gather(i.Ticker, i.Amount / output))))
        .concat([{ buildingTime, materialCount }])
        .reduce((x, y) => ({
            buildingTime: mergeAdd(x.buildingTime, y.buildingTime),
            materialCount: mergeAdd(x.materialCount, y.materialCount)
        }), { buildingTime: {}, materialCount: {} });
}

function mergeAdd(left: { [name: string]: number }, right: { [name: string]: number }) {
    const result: { [name: string]: number } = {};
    new Set<string>(Object.keys(left).concat(Object.keys(right)))
        .forEach(name => result[name] = (left[name] ?? 0) + (right[name] ?? 0));
    return result;
}

const classes = StyleSheet.create({
    material: {
        display: "flex",
        flexDirection: "column",
        "align-items": "center",
        "justify-content": "center",
        margin: "14px",
    }
});