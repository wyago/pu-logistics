import { css } from "aphrodite";
import { h } from "maquette";
import { Component } from "../../types";
import { globalClasses } from "../globalClasses";

export function createAboutPage(): Component {
    return {
        render: () => h("div." + css(globalClasses.page), [
            h("h1", {key:Math.random()}, ["About"]),
            h("p",{key:Math.random()},  [
                "This is an early set of tools using the ", 
                h("a", {href: "https://doc.fnar.net/"}, ["FIO REST API"]),
                " to give general overview information applicable to any player. If prices look off or something is ",
                "going wrong, go to the settings page to clear the cache."
            ]),
            h("p", {key:Math.random()}, [
                "You can find a list of other tools here: ",
                h("a", {href: "https://handbook.apex.prosperousuniverse.com/wiki/community-resources/#tools"}, ["https://handbook.apex.prosperousuniverse.com/wiki/community-resources/#tools"])
            ]),
            h("p", {key:Math.random()}, [
                "Development of tools for Prosperous Universe is discussed at this discord: ",
                h("a", {href: "https://discord.gg/2MDR5DYSfY"}, ["https://discord.gg/2MDR5DYSfY"])
            ]),
        ])
    }
}