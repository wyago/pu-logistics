import { css } from "aphrodite";
import { h } from "maquette";
import { materials, workforces } from "../..";
import { Component } from "../../types";
import { card } from "../card";
import { exchangeSetter } from "../exchange";
import { toggle } from "../form/toggle";
import { currency, formatted } from "../formatting";
import { globalClasses } from "../globalClasses";
import { material } from "../material";
import { price } from "../../model/price";
import { shimmer } from "../shimmer";

let consumption: { [workforce: string]: { [ticker: string]: boolean } } | undefined = undefined;

function requireConsumptionInitialized() {
    if (!consumption) {
        consumption = JSON.parse(localStorage.getItem("consumption") || "{}");
        if (Object.keys(consumption!).length === 0) {
            Object.keys(workforces)
                .forEach(name => (consumption![name] = {}, workforces[name].Needs.forEach(n => consumption![name][n.MaterialTicker] = true)));
        }
    }
}

function saveConsumption() {
    localStorage.setItem("consumption", JSON.stringify(consumption));
}

export async function getConsumption(workforce: string): Promise<number> {
    const prices = await Promise.all(
        workforces[workforce].Needs
        .map(n => 
            price(n.MaterialTicker).then(p => [n.MaterialTicker, p * n.Amount] as const)));
            
    const materialPrices: { [ticker: string]: number } = {};
    prices.forEach(pair => materialPrices[pair[0]] = pair[1]);
    
    requireConsumptionInitialized();

    return workforces[workforce]
        .Needs
        .map(n => consumption![workforce][n.MaterialTicker] ? materialPrices[n.MaterialTicker] : 0)
        .reduce((x, y) => x + y, 0);
}

export function getEfficiency(workforce: string): number {
    requireConsumptionInitialized();

    const luxuries = workforces[workforce]
        .Needs
        .filter(n => n.MaterialCategory === "8a0bd8b6a329c3d632da9da63c818b3d" && consumption![workforce][n.MaterialTicker])
        .length;
    
    return luxuries === 2
        ? 1 
        : luxuries === 1 
        ? 0.92
        : 0.79;
}

function workforce(name: string): Component {
    const prices = Promise.all(
        workforces[name].Needs
        .map(n => 
            price(n.MaterialTicker).then(p => [n.MaterialTicker, p * n.Amount] as const)))
            .then(prices => {
                const materialPrices: { [ticker: string]: number } = {};
                prices.forEach(pair => materialPrices[pair[0]] = pair[1]);
                return materialPrices;
            });

    return shimmer(name, "200px", "calc(56px + 2em + 20px)", prices.then(materialPrices => ({
        render() { 
            requireConsumptionInitialized();
            const needs = workforces[name].Needs;
            needs.sort((a, b) => -a.MaterialCategory.localeCompare(b.MaterialCategory))
            const toggles = needs
                .filter(n => n.MaterialCategory === "8a0bd8b6a329c3d632da9da63c818b3d")
                .map(n => ({
                    ...toggle({
                        key: n,
                        defaultEnabled: consumption![name][n.MaterialTicker],
                        disabled: materials[n.MaterialTicker].CategoryName === "consumables (basic)",
                        label: material(n.MaterialTicker),
                        onchange: v => { consumption![name][n.MaterialTicker] = v; saveConsumption() }
                    }),
                    value: materialPrices[n.MaterialTicker]
                }));
            return [
                h("h2", {key:name}, [name]),
                h("div." + css(globalClasses.horizontal), {key: name + "cost"}, [
                    h("label", { styles: { marginRight: "8px" } }, ["Cost: "]),
                    formatted(toggles.map(t => t.enabled() ? t.value : 0).reduce((x, y) => x + y, 0)),
                    " ",
                    currency(),
                    " a day per 100"
                ]),
                h("div." + css(globalClasses.horizontal), { styles: { justifyContent: "space-evenly" }, key: name + "toggles"}, [
                    toggles.map(t => t.render())
                ]),
            ]
        }
    })));
}

export function createConsumptionPage(): Component {
    let toggles: Component[] = [];
    function update() {
        toggles = ["PIONEER", "SETTLER", "TECHNICIAN", "ENGINEER", "SCIENTIST"].map(workforce);
    }
    update();

    const changeExchange = exchangeSetter(update);

    return {
        render: () => h("div." + css(globalClasses.page), {}, [
            h("h1", ["Consumption settings"]),
            h("p", [
                "Select the consumables you are providing to you workers."
            ]),
            h("div." + css(globalClasses.horizontal), {key: "ex"}, [
                h("label", { styles: { width: (14 * 5) + "px", marginBottom: "14px"} }, ["Exchange: "]),
                changeExchange.render(),
            ]),
            card({ styles: { width: "fit-content" }}, toggles.map(t => t.render()))
        ])
    }
}
