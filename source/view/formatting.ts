import { exchanges } from "..";
import { exchange } from "./exchange";

const small = Intl.NumberFormat("en-US", { maximumSignificantDigits: 2, minimumSignificantDigits: 1  });
const big = Intl.NumberFormat("en-US", { maximumFractionDigits: 2 });
const integer = Intl.NumberFormat("en-US", { maximumFractionDigits: 0 });

export function formatted(n: number) {
    const format = n > 1 ? big : small;
    if (n > 1000000000) {
        return format.format(n / 1000000000) + "B"
    }
    if (n > 1000000) {
        return format.format(n / 1000000) + "M"
    }
    if (n > 1000) {
        return format.format(n / 1000) + "k"
    }
    if (n < -1000000000) {
        return format.format(n / 1000000000) + "B"
    }
    if (n < -1000000) {
        return format.format(n / 1000000) + "M"
    }
    if (n < -1000) {
        return format.format(n / 1000) + "k"
    }
    return format.format(n);
}

export function formattedInteger(n: number) {
    const format = integer;
    if (n > 1000000000) {
        return format.format(n / 1000000000) + "B"
    }
    if (n > 1000000) {
        return format.format(n / 1000000) + "M"
    }
    if (n > 1000) {
        return format.format(n / 1000) + "k"
    }
    if (n < -1000000000) {
        return format.format(n / 1000000000) + "B"
    }
    if (n < -1000000) {
        return format.format(n / 1000000) + "M"
    }
    if (n < -1000) {
        return format.format(n / 1000) + "k"
    }
    return format.format(n);
}

export function currency() {
    return exchanges.find(e => e.ExchangeCode === exchange)!.CurrencyCode;
}

export function time(days: number) {
    const hours = ~~(days*24);
    days = days - hours / 24;
    const minutes = ~~(days*24*60);
    if (hours && minutes) {
        return hours + "h " + minutes + "m";
    } else if (hours) {
        return hours + "h";
    } else {
        return minutes + "m";
    }
}