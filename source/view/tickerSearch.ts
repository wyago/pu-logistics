import { css, StyleSheet } from 'aphrodite';
import { h, VNode, VNodeChild } from "maquette";

function escapeRegex(text: string) {
    return text.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

export function tickerSearch(def: string, lists: { Name: string, Ticker: string }[], onchange: (name: string) => void): () => VNode {
    function result(o: { Name: string, Ticker: string }) {
        return h("div." + css(styles.result), {
            key: o, 
            onmousedown: () => {value = o.Ticker, onchange(o.Ticker)}}, 
            o.Ticker === o.Name ? [o.Name] : [o.Ticker + " - " + o.Name])
    }
    let results: VNodeChild[] = lists.slice(0, 50).map(result);
    let focused = false;
    let value = def;
    return () => {
        return h("div." + css(styles.search), [
            h("input", { 
                value,
                onfocus: () => focused = true,
                onblur: () => focused = false,
                onkeyup: e => {
                    value = (e.target as HTMLInputElement).value;
                    onchange((e.target as HTMLInputElement).value);
                    results = lists
                        .filter(mat =>
                            mat.Ticker.match(new RegExp(".*" + escapeRegex((e.target as HTMLInputElement).value) + ".*", "i")) ||
                            mat.Name.match(new RegExp(".*" + escapeRegex((e.target as HTMLInputElement).value) + ".*", "i")))
                        .map(result);
                    results = results.slice(0, 50);
                }}),
            results.length > 0 && focused ? h("div." + css(styles.searchresults), results) : []
        ]);
    };
}

const styles = StyleSheet.create({
    search: {
        positon: "relative",
        margin: "0 14px",
    },
    searchresults: {
        zIndex: 1,
        "position": "absolute",
        "background-color": "#fff",
        "border": "solid 1px #aaa",
        "box-shadow": `
        0 3.9px 5px rgba(0, 0, 0, 0.017),
        0 10.9px 13.9px rgba(0, 0, 0, 0.025),
        0 26.2px 33.5px rgba(0, 0, 0, 0.033),
        0 87px 111px rgba(0, 0, 0, 0.05)
      `,
    },
    result: {
        padding: "4px"
    }
});
