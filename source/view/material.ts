import { css, StyleSheet } from "aphrodite";
import { h, VNode } from "maquette";
import { buildings, materials } from "..";
import { Material } from "../coreTypes";
import { setMaterialModal } from "./modals/materialView";

function rgb(r: number, g: number, b: number) {
    return [r,g,b];
}

function darker(c: number[]) {
    return c.map(x => x - 30);
}

function back(c: number[]) {
    return "rgb(" + c[0] + "," + c[1] + "," + c[2] +")";
}

const colorMap: { [category: string]: number[] } = {
    'generic': rgb(179, 255, 255),
    'chemicals': rgb(183, 46, 91),
    'agricultural products': rgb(117, 43, 43),
    'ship kits': rgb(29, 36, 16),
    'drones': rgb(91, 46, 183),
    'utility': rgb(54, 54, 54),
    'liquids': rgb(80, 41, 23),
    'elements': rgb(91, 46, 183),
    'ship parts': rgb(59, 45, 148),
    'gases': rgb(67, 77, 87),
    'construction prefabs': rgb(54, 54, 54),
    'fuels': rgb(30, 123, 30),
    'unit prefabs': rgb(59, 45, 148),
    'electronic parts': rgb(92, 30, 122),
    'energy systems': rgb(51, 24, 216),
    'medical equipment': rgb(9, 15, 15),
    'alloys': rgb(77, 77, 77),
    'construction materials': rgb(24, 91, 211),
    'minerals': rgb(73, 85, 97),
    'construction parts': rgb(35, 30, 68),
    'ship engines': rgb(14, 57, 14),
    'consumables (luxury)': rgb(9, 15, 15),
    'software tools': rgb(6, 6, 29),
    'consumables (basic)': rgb(73, 85, 97),
    'metals': rgb(16, 92, 87),
    'electronic devices': rgb(86, 20, 147),
    'electronic pieces': rgb(73, 85, 97),
    'ship shields': rgb(132, 82, 34),
    'electronic systems': rgb(49, 24, 7),
    'plastics': rgb(26, 60, 162),
    'textiles': rgb(80, 41, 23),
    'software systems': rgb(26, 60, 162),
    'software components': rgb(67, 77, 87),
    'ores': rgb(57, 95, 96)
}

export function isFundamental(mat: Material): boolean {
    return mat.CategoryName === "ores" || mat.CategoryName === "minerals" || mat.CategoryName === "gases" || mat.CategoryName === "liquids";
}

const classes = StyleSheet.create({
    material: {
        width: '56px',
        height: '56px',
        display: "flex",
        "align-items": "center",
        "justify-content": "center",
        "margin": "14px",
        "font-size": "18px", 
        "border-radius": "6px",
        position: "relative",
        '@media (max-aspect-ratio: 2/3)': {
            margin: "6px",
            width: "40px",
            height: "40px"
        },
        cursor: "pointer"
    },
    warning: {
        position: "absolute",
        right: "-11px",
        top: "-11px",
    },
    count: {
        position: "absolute",
        left: "-11px",
        top: "-11px",
        borderRadius: "100px",
        border: "2px solid #000",
        backgroundColor: "#fff",
        height: "24px",
        minWidth: "24px",
        paddingLeft: "2px",
        paddingRight: "2px",
        color: "#000",
        textAlign: "center",
        lineHeight: "20px",
        fontSize: "14px"
    }
})

export function material(ticker: string, warn = false, count = "1"): VNode {
    const mat = materials[ticker]
    const color = mat ? colorMap[mat.CategoryName] : colorMap["generic"];
    const styles = {
        'background-color': back(color),
        color: "#fff"
    };

    ticker = ticker.slice(0, 3);

    function onclick() {
        setMaterialModal(ticker);
    }

    if (color[0] + color[1] + color[2] > 400) {
        styles.color = "#000";
    }
    if (mat) {
        return h("div." + css(classes.material), { key: mat.Ticker, onclick, styles, title: mat.CategoryName + ": " + mat.Name }, [
            mat.Ticker,
            warn ? h("img." + css(classes.warning), { src: "/icon/alert-circle.svg", title: "This is not on the market" }) : [],
            count !== "1" ? h("div." + css(classes.count), [count]) : []
        ]);
    } if (buildings[ticker]) {
        return h("div." + css(classes.material), { key: ticker, onclick, styles, title: buildings[ticker].Expertise + ": " + buildings[ticker].Name }, [
            ticker,
            warn ? h("img." + css(classes.warning), { src: "/icon/alert-circle.svg", title: "This is not on the market" }) : [],
            count !== "1" ? h("div." + css(classes.count), [count]) : []
        ]);
    } else {
        return h("div." + css(classes.material), { key: ticker, onclick, styles }, [
            ticker,
            warn ? h("img." + css(classes.warning), { src: "/icon/alert-circle.svg", title: "This is not on the market" }) : [],
            count !== "1" ? h("div." + css(classes.count), [count]) : []
        ]);
    }
}