import { createProjector } from "maquette";

export const globalProjector = createProjector();
(window as any).globalProjector = globalProjector;

addEventListener("popstate", () => {
    globalProjector.scheduleRender();
});