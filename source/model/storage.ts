export const version = "3";

if (localStorage.getItem("version") !== version) {
    localStorage.clear();
    localStorage.setItem("version", version);
}

export function clearStorage() {
    localStorage.clear();
    localStorage.setItem("version", version);
    location.reload();
}