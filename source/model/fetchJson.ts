
export function fetchJson<T>(uri: string, def?: T): Promise<T> {
    return fetch(uri)
        .then(x => x.json())
        .catch(_ => def);
}