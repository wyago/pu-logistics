import { cache } from "./cache";
import { fetchJson } from "./fetchJson";
import { exchange } from "../view/exchange";

type ExchangeTicker = {
    Supply: number,
    Demand: number,
    PriceAverage: number,
    Bid: number,
    Ask: number,
    Currency: string,
};

export type Chart = {
    Open: number,
    Close: number,
    Volume: number,
    Traded: number,
    TimeEpochMs: number,
}[];

const now = Date.now();

export const unavailable = cache("unavailable", (ticker: string, innerExchange?: string) => {
    innerExchange = innerExchange || exchange;
    return {
        id: ticker + "." + innerExchange,
        value: () => fetchJson<ExchangeTicker>("https://rest.fnar.net/exchange/" + ticker + "." + innerExchange).then(ex => ({
            bid: ex.Demand === 0,
            ask: ex.Supply === 0 
        }))
    };
});

export const price = cache("price", (ticker: string, innerExchange?: string) => {
    innerExchange = innerExchange || exchange;
    return {
        id: ticker + "." + innerExchange,
        value: async () => {
            const chart = await fetchJson<Chart>("https://rest.fnar.net/exchange/cxpc/" + ticker + "." + innerExchange, []);
            let filtered = chart.filter(c => c.TimeEpochMs > (now - 1000*60*60*24*4));
        
            if (filtered.length < 5){
                filtered = [];
                for (let i = chart.length - 1; i >= chart.length - 5 && i >= 0; --i) {
                    filtered.push(chart[i]);
                }
            }
            
            if (filtered.length === 0) {
                return await (await fetchJson<ExchangeTicker>("https://rest.fnar.net/exchange/" + ticker + "." + innerExchange)).PriceAverage;
            }
        
            let count = 0;
            let priceTimesCount = 0;
            for (const column of filtered) {
                count += column.Traded;
                priceTimesCount += column.Volume;
            }
        
            return priceTimesCount / count;
        }
    }
});