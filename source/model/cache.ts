export const cacheVersion = "2";

if (localStorage.getItem("cacheVersion") !== cacheVersion) {
    clearCache();
    localStorage.setItem("cacheVersion", cacheVersion);
}

export function clearCache() {
    for (let i = 0; i < localStorage.length; ++i) {
        const key = localStorage.key(i);
        if (key?.startsWith("cached-")) {
            localStorage.removeItem(key);
        }
    }
    window.location.reload();
}

export function cache<T, I extends any[]>(name: string, call: (...i: I) => { id: string, value: () => Promise<T> }): 
    (...i: I) => Promise<T> {
    let inProgress: { [id: string]: { time: number, value: Promise<T> } | undefined } = {};

    const stored = localStorage.getItem("cached-" + name);
    if (stored) {
        const values = JSON.parse(stored) as { [id: string]: { time: number, value: T } };
        for (const name of Object.keys(values)) {
            inProgress[name] = {
                time: values[name].time,
                value: Promise.resolve(values[name].value)
            };
        }
    }

    let timer = -1;

    return (...args) => {
        const { id, value } = call(...args);
        if (!inProgress[id] || inProgress[id]!.time < Date.now() - 1000*60) {
            inProgress[id] = {
                time: Date.now(),
                value: value().then(x => {
                        if (timer > -1) {
                            window.clearTimeout(timer);
                        }
                        timer = window.setTimeout(
                            async () => {
                                const toStore: { [id: string]: { time: number, value: T } } = {};
                                for (const name of Object.keys(inProgress)) {
                                    if (inProgress[name]) {
                                        toStore[name] = {
                                            time: inProgress[name]!.time,
                                            value: await inProgress[name]!.value
                                        };
                                    }
                                }
                                localStorage.setItem("cached-" + name, JSON.stringify(toStore));
                            },
                            2000
                        );
                        return x;
                    })
                };
        }
        return inProgress[id]!.value;
    };
}