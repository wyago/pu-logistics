import { Project } from "./project";

type ProjectSet = { [name: string]: Project };

const undoStack: ProjectSet[] = [];
let projects: ProjectSet = localStorage.getItem("projects") ? JSON.parse(localStorage.getItem("projects")!) : [];
const redoStack: ProjectSet[] = [];

export function getProjects() {
    return projects;
}

export function setProjects(next: ProjectSet) {
    undoStack.push(projects);
    projects = next;
}

export function undoProjects() {
    if (undoStack.length === 0) {
        return;
    }
    redoStack.push(projects);
    projects = undoStack.pop()!;
}

export function redoProjects() {
    if (redoStack.length === 0) {
        return;
    }
    undoStack.push(projects);
    projects = redoStack.pop()!;
}

