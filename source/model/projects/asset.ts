export type Asset = {
    kind: "cash",
    code: string,
    amount: number,
} | {
    kind: "material",
    ticker: string,
    amount: number,
    location: string
} | {
    kind: "building",
    ticker: string,
    amount: number,
    location: string
};

export function merge(left: Asset, right: Asset): Asset | undefined {
    if (left.kind === "cash" && right.kind === "cash" && left.code === right.code) {
        return {
            kind: "cash",
            code: left.code,
            amount: left.amount + right.amount
        };
    } else if (left.kind === "material" && right.kind === "material" && left.ticker === right.ticker && left.location === right.location) {
        return {
            kind: "material",
            amount: left.amount + right.amount,
            location: left.location,
            ticker: left.ticker
        };
    } else if (left.kind === "building" && right.kind === "building" && left.ticker === right.ticker && left.location === right.location) {
        return {
            kind: "building",
            amount: left.amount + right.amount,
            location: left.location,
            ticker: left.ticker
        };
    }
    return undefined;
}