import { exchanges } from "../..";
import { Recipe } from "../../coreTypes";
import { Asset } from "./asset";

export type Action = {
    kind: "declare",
    asset: Asset,
    price?: number,
} | {
    kind: "buy",
    ticker: string,
    amount: number,
    location: string,
    price: number,
} | {
    kind: "sell",
    ticker: string,
    amount: number,
    location: string,
    price: number,
} | {
    kind: "make",
    recipe: Recipe,
    count: number,
    location: string,
    efficiency: number,
} | {
    kind: "transport",
    assets: Asset[],
    destination: string,
    stlFuel?: number,
    stlPrice: number,
    ftlFuel?: number,
    ftlPrice: number,
    time?: number,
} | {
    kind: "build",
    ticker: string,
    location: string,
};

export function actionDuration(action: Action) {
    if (action.kind === "make") {
        return action.recipe.TimeMs / 1000 / 60 / 60 / 24;
    } else if (action.kind === "transport") {
        return action.time || 0;
    }

    return 0;
}

export function actionOutputs(action: Action): Asset[] {
    if (action.kind === "declare") {
        return [action.asset];
    } else if (action.kind === "buy") {
        return [{
            kind: "material",
            amount: action.amount,
            ticker: action.ticker,
            location: action.location
        }];
    } else if (action.kind === "sell") {
        return [{
            kind: "cash",
            amount: action.amount * action.price,
            code: exchanges.find(cx => cx.ExchangeName === action.location)!.CurrencyCode,
        }];
    } else if (action.kind === "make") {
        return action.recipe.Outputs.map(o => ({
            kind: "material",
            amount: o.Amount,
            location: action.location,
            ticker: o.Ticker
        }));
    } else if (action.kind === "transport") {
        return action.assets.map(a => ({
            ...a,
            location: action.destination
        }));
    } else if (action.kind === "build") {
        return [{
            kind: "building",
            location: action.location,
            ticker: action.ticker,
            amount: 1
        }]
    }
    return [];
}