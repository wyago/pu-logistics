import { Action, actionDuration, actionOutputs } from "./action";
import { Asset, merge } from "./asset";

export type Step = {
    actions: Action[]
};

export function stepDuration(step: Step) {
    let max = 0;
    for (const action of step.actions) {
        max = Math.max(actionDuration(action), max);
    }
    return max;
}

export function stepOutputs(step: Step) {
    const scattered = ([] as Asset[]).concat(...step.actions.map(action => actionOutputs(action)));

    const result: Asset[] = [];
    for (const x of scattered) {
        let wasMerged = false;
        for (let i = 0; i < result.length; ++i) {
            const merged = merge(result[i], x);

            if (merged !== undefined) {
                result[i] = merged;
                wasMerged = true;
                break;
            } 
        }
        if (!wasMerged) {
            result.push(x);
        }
    }

    return result;
}