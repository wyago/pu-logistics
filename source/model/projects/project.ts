import { Step, stepDuration } from "./step";

export type Project = {
    name: string,
    steps: Step[]
};

export function projectDuration(project: Project) {
    return project.steps.reduce((x, y) => x + stepDuration(y), 0)
}