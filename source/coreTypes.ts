export type Material = {
  CategoryName: string;
  CategoryId: string;
  Name: string;
  MatId: string;
  Ticker: string;
  Weight: number;
  Volume: number;
  UserNameSubmitted: string;
  Timestamp: string;
}

export type Workforce = {
  Needs: {
    MaterialId: string;
    MaterialName: string;
    MaterialTicker: string;
    MaterialCategory: string;
    Amount: number;
  }[];
  WorkforceType: string;
}

export type Recipe = {
  BuildingTicker: string;
  RecipeName: string;
  Inputs: {
      Ticker: string;
      Amount: number;
  }[];
  Outputs: {
      Ticker: string;
      Amount: number;
  }[];
  TimeMs: number;
}

export type Exchange = {
  ExchangeId: string;
  ExchangeName: string;
  ExchangeCode: string;
  ExchangeOperatorId: null;
  ExchangeOperatorCode: null;
  ExchangeOperatorName: null;
  CurrencyNumericCode: number;
  CurrencyCode: string;
  CurrencyName: string;
  CurrencyDecimals: 2;
  LocationId: string;
  LocationName: string;
  LocationNaturalId: string;
}

export type Listing = {
  MaterialTicker: string;
  ExchangeCode: string;
  MMBuy: null;
  MMSell: null;
  PriceAverage: number;
  AskCount: null;
  Ask: null;
  Supply: number;
  BidCount: null;
  Bid: null;
  Demand: number;
}

export type Building = {
    BuildingCosts: {
      CommodityName: string;
      CommodityTicker: string;
      Weight: number;
      Volume: number;
      Amount: number;
    }[];
    Recipes: unknown[];
    Name: string;
    Ticker: string;
    Expertise: string;
    Pioneers: number;
    Settlers: number;
    Technicians: number;
    Engineers: number;
    Scientists: number;
    AreaCost: number;
    UserNameSubmitted: string;
    Timestamp: string;
}

export type Popi = {
    BuildingCosts: {
        CommodityName: string;
        CommodityTicker: string;
        Weight: number;
        Volume: number;
        Amount: number;
    }[];
    Name: string;
    Ticker: string;
    Expertise: null;
    Pioneers: number;
    Settlers: number;
    Technicians: number;
    Engineers: number;
    Scientists: number;
    AreaCost: number;
    UserNameSubmitted: string;
    Timestamp: string;
}

export type Planet = {
    Name: string;
    Id: string;
    PopulationId: string;
    Infrastructure: {

    };
    Resources: {
        MaterialId: string,
        ResourceType: "MINERAL" | "GASEOUS" | "LIQUID",
        Factor: number
    }[];
}