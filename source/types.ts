import { VNodeChild } from "maquette";

export type Component = { render(): VNodeChild };