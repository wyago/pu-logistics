import { Building, Exchange, Material, Planet, Popi, Recipe, Workforce } from './coreTypes';
import { globalProjector } from "./global";
import { fetchJson } from './model/fetchJson';
import { createMainPane } from "./view/mainPane";
import { createMenu } from "./view/menu";
import { createMaterialView } from './view/modals/materialView';
import { root } from "./view/root";

/*
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', {scope: './'}).then(function(registration) {
        console.log('Service worker registration succeeded:', registration);
    }, function(error) {
        console.log('Service worker registration failed:', error);
    });
}
*/
const menu = createMenu();
export let materials: { [name: string]: Material } = {};
export let materialsById: { [name: string]: Material } = {};
export let buildings: { [name: string]: Building } = {};
export let workforces: { [name: string]: Workforce } = {};
export let workforceList: Workforce[] = undefined!;
export let popis: { [name: string]: Popi } = {};
export let materialList: Material[] = undefined!;
export let buildingList: Building[] = undefined!;
export let popiList: Popi[] = undefined!;
export let recipes: Recipe[]= undefined!;
export let exchanges: Exchange[] = undefined!;
export let planetList: Planet[] = undefined!;
export let planetByNaturalId: { [name: string]: Planet } = {};

(async () => {
    [materialList, buildingList, popiList, planetList, workforceList] = await Promise.all([
        fetchJson<any>("/data/mats.json"), 
        fetchJson<any>("/data/buildings.json"), 
        fetchJson<any>("/data/planetaryprojectbuildings.json"), 
        fetchJson<any>("/data/planetNames.json"),
        fetchJson<any>("/data/needs.json"),
    ]);
    planetList.sort((a, b) => a.Id.localeCompare(b.Id));
    for (const planet of planetList) {
        planetByNaturalId[planet.Id] = planet;
    }
    for (const material of materialList) {
        materials[material.Ticker] = material;
        materialsById[material.MatId] = material;
    }
    for (const building of buildingList) {
        buildings[building.Ticker] = building;
    }
    for (const workforce of workforceList) {
        workforce.Needs.sort((a, b) => a.MaterialTicker.localeCompare(b.MaterialTicker));
        workforces[workforce.WorkforceType] = workforce;
    }
    for (const popi of popiList) {
        popis[popi.Ticker] = popi;
    }
    recipes = await fetchJson<any>("/data/recipes.json");
    for (const recipe of recipes) {
        recipe.Inputs = recipe.Inputs || [];
    }
    exchanges = await fetchJson<any>("/data/cxl.json");

    document.getElementsByClassName("loading")[0].remove();
    const pane = createMainPane();
    const materialView = createMaterialView();
    globalProjector.append(document.body, () => root([menu(), pane(), materialView.render()]));
})();

window.onerror = (e) => alert("Sorry, the app crashed!");