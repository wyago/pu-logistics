//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    devtool: 'inline-source-map',
    devServer: {
        contentBase: "./public",
        port: 80,
        historyApiFallback: true,
        allowedHosts: [
            'dev.leksogur.com'
        ],
        historyApiFallback: true
        //host: "0.0.0.0",
    },
    plugins: [
        new webpack.DefinePlugin({
            BUILD_TIME: Date.now().toString(),
            LOCAL_INSTRUMENTS: true,
        }),
        //new BundleAnalyzerPlugin(),
    ],
});
